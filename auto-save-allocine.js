// ==UserScript==
// @name         text-area-autosave
// @version      0.1.2
// @description  Try to autosave on each textArea (Allocine's CMS edition)
// @author       Axel Delcros
// @match        https://cms.allocine.net/fr/News/Edit/*
// @downloadURL  https://gitlab.com/adelcros/tampermonkey-auto-save-script/auto-save-allocine.js
// @grant        GM.setValue
// @grant        GM.getValue
// @grant        GM.deleteValue
// @grant        GM.listValues
// ==/UserScript==

(() => {
  "use strict";
  setTimeout(async () => {
    const DAYS_BEFORE_FLUSHING = 7; // How much time wee keep local saves
    const MS_TO_DAY = 86400000; // How many miliseconds is 1 day
    const currentHref = window.location.href;
    const postId = currentHref.split("/")[5]; // get the Article unique ID

    // List all values and check if we have to remove some old stuffs (Decrease localStorage usage)
    const allValues = await GM.listValues();
    if (allValues && allValues.length > 0) {
      let oldValues = [];
      for (let value of allValues) {
        const valueParsed = JSON.parse(await GM.getValue(value));
        const { date } = valueParsed;
        if (Date.now() > date + DAYS_BEFORE_FLUSHING * MS_TO_DAY) {
          await GM.deleteValue(value);
        } else {
          oldValues.push({
            name: value,
            value: valueParsed,
          });
        }
      }
    }

    const iframe = document.getElementById("Description_ifr");
    const iframeBody = iframe.contentWindow.document.getElementById("tinymce");

    if (iframeBody) {
      const oldValue = await GM.getValue(`textarea-${postId}`);

      // If there is already a value bind to this textarea at load of the page, replace the inner text with saved one
      if (oldValue) {
        const { date, value, href } = JSON.parse(oldValue) || {};

        // Check inside the saved value if we are on the right article
        if (currentHref === href) {
          iframeBody.innerHTML = value;
        }
      }

      // Listen input events on this textarea => Each input overwrite the saved value
      iframeBody.addEventListener("input", (e) => {
        const newValue = {
          href: currentHref,
          date: Date.now(),
          value: iframeBody.innerHTML,
        };
        const transformed = JSON.stringify(newValue);
        GM.setValue(`textarea-${postId}`, transformed);
      });
    }
  }, 3000);
})();
