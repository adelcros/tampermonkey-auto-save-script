# tampermonkey-auto-save-script for Allocine

A little script developped in order to auto-save every articles / posts immediately on greaseMonkey / tamperMonkey localstorage.

## How to use it

1.  Install [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) extension on Chrome web store
2. Create new script on TamperMonkey app
3. Copy / Paste the content of the js file on it
4. Save it (cms + s / ctrl + s)
5. Refresh your target page
